FROM node:8

ADD npm.lock /npm.lock
ADD package.json /package.json

ENV NODE_PATH = /node_modules
ENV PATH=$PATH:/node_modules/.bin
ENV npm

WORKDIR /app
ADD . /app

EXPOSE 3000
EXPOSE 35729

ENTRXPOINT ["/bin/bash","/app/run.sh"]
CMD ["start"]