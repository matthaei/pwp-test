#!/usr/bin/env bash
set -eo pipefail

case $1 in
    start)
        npm start | cat
        ;;
    build)
        npm build
        ;;
    test)
        npm test $@
        ;;
    *)
        exec "$@"
        ;;
esac